
#ifndef _TRIANGLE_HPP_
#define _TRIANGLE_HPP_

/*****************************************************************************/
#include <iostream>
#include "point.hpp"
#include <math.h>

/*****************************************************************************/


class Triangle
{
private:
    Point point1, point2, point3;
//    double a_x1, a_y1, b_x2 ,b_y2, c_x3, c_y3;
//    double calculateLenght(Point point1, Point point2);
public:

    Triangle(double a_x,double a_y,double b_x,double b_y,double c_x,double c_y);
    Triangle(Point  obj1, Point  obj2, Point  obj3);
    
    const Point & getPoint1() const;
    const Point & getPoint2() const;
    const Point & getPoint3() const;
    
    double getSide12Length() const;
    double getSide13Length() const;
    double getSide23Length() const;
    
    double getPerimeter() const;
    double getArea() const;
    
    bool operator == (Triangle _p) const;
    bool operator != (Triangle _p) const;
    
    double calculateAngle();
    
    double getAngle1();
    double getAngle2();
    double getAngle3();
    
    bool isRectangular();
    bool isIsosceles();
    bool isEquilateral();
};
inline const Point & Triangle::getPoint1()const
{
    return point1;
}
inline const Point & Triangle::getPoint2()const
{
    return point2;
}
inline const Point & Triangle::getPoint3()const
{
    return point3;
}


/*****************************************************************************/

#endif //  _TRIANGLE_HPP_

