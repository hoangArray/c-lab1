// (C) 2013-2015, Sergei Zaychenko, KNURE, Kharkiv, Ukraine

#include "triangle.hpp"
//#include <math.h>
double pi = 3.1415926535;
struct TriangleLength {
    double leight1, leight2, leight3;
public:
    TriangleLength(double _length1, double _length2, double _length3) {
        this->leight1 = _length1;
        this->leight2 = _length2;
        this->leight3 = _length3;
    }
    
    static TriangleLength calculate(const Triangle & _triangle) {
        double AB,AC,BC;
        Point point1 = _triangle.getPoint1();
        Point point2 = _triangle.getPoint2();
        Point point3 = _triangle.getPoint3();
        
        AB = round(pow(point2.m_x - point1.m_x , 2) + (pow(point2.m_y - point1.m_y, 2)));
        AC = round(pow(point3.m_x - point1.m_x , 2) + (pow(point3.m_y - point1.m_y, 2)));
        BC = round(pow(point3.m_x - point2.m_x , 2) + (pow(point3.m_y - point2.m_y, 2)));
        
        return TriangleLength(AB, AC, BC);
    }
};


Triangle::Triangle(double a_x,double a_y,double b_x,double b_y,double c_x,double c_y)
:point1(a_x, a_y), point2(b_x, b_y), point3(c_x, c_y)//a_x1(a_x) ,a_y1(a_y),b_x2(b_x),b_y2(b_y),c_x3(c_x),c_y3(c_y)
{
//    point1 = Point(a_x, a_y);
//    point2 = Point(b_x, b_y);
//    point3 = Point(c_x, c_y);
}
Triangle::Triangle(Point obj1, Point  obj2, Point  obj3)
:point1(obj1),point2(obj2),point3(obj3)
{
    
}

double  Triangle::getSide12Length() const
{
    //AB |AB|=√(xB−xA)^2+(yB−yA)^2,result1 =sqrt(pow(b_x2 - a_x1, 2) + pow(b_y2 - a_y1, 2));
    double result1;
    result1 = sqrt( pow(point2.m_x - point1.m_x , 2) + (pow(point2.m_y - point1.m_y, 2)));
    return result1;
}

double Triangle::getSide13Length() const
{
    //AC |AC|=√(xC−xA)^2+(yC−yA)^2,
    double result2;
    result2 = sqrt( pow(point3.m_x - point1.m_x , 2) + (pow(point3.m_y - point1.m_y, 2)));
    return result2;
    
}

double Triangle::getSide23Length() const
{
    //|BC|=(xC−xB)2+(yC−yB)2
    double result3;
    result3 = sqrt( pow(point3.m_x - point2.m_x , 2) + (pow(point3.m_y - point2.m_y, 2)));
    return result3;
}

double Triangle::getPerimeter() const
{
    double perimeter;
    perimeter = getSide12Length() + getSide13Length() + getSide23Length();
    return perimeter;
}

double  Triangle::getArea() const
{
    //    Фо́рмула Герона позволяет вычислить площадь треугольника (S) по его сторонам a, b, c:
    //    S={\sqrt {p(p-a)(p-b)(p-c)}},} S={\sqrt {p(p-a)(p-b)(p-c)}},
    //    где p — полупериметр треугольника: {\displaystyle p={\frac {a+b+c}{2}}} p={\frac {a+b+c}{2}}.
    double halfPerimeter = getPerimeter()/2;
    double square;
    square = sqrt(halfPerimeter*(halfPerimeter - getSide12Length())*
                  (halfPerimeter - getSide13Length())*(halfPerimeter - getSide23Length()));
    return square;
}

bool Triangle::operator == (Triangle _p) const
{
   return   point1 == _p.getPoint1() &&
            point2 == _p.getPoint2()&&
            point3 == _p.getPoint3();
}

bool Triangle::operator != (Triangle _p) const
{
    return !(*this == _p);
}

double Triangle::calculateAngle()
{
    double result1 = ((pow(getSide12Length(),2) + pow(getSide23Length(), 2) - pow(getSide13Length(), 2)))
    /2*getSide12Length()*getSide23Length();
    
    double aCos = acos(result1)/2;
    return aCos;
}

double Triangle::getAngle1()
{
    return  calculateAngle();
}

double Triangle::getAngle2()
{
    double result1 = ((pow(getSide12Length(),2) + pow(getSide23Length(), 2) - pow(getSide13Length(), 2)))
    /2*getSide12Length()*getSide23Length();
    double aCos = acos(result1);
    return aCos;
}
double Triangle::getAngle3()
{
    return calculateAngle();
}

bool Triangle::isRectangular()
{
//    Triangle obj(point1,point2,point3);
    TriangleLength length = TriangleLength::calculate(*this);
    if(length.leight1 + length.leight3 == length.leight2)
        return true;
    else
        return false;
}

bool Triangle::isIsosceles()
{
    TriangleLength length = TriangleLength::calculate(*this);
    if(length.leight1 == length.leight2 ||
       length.leight3 == length.leight1 ||
       length.leight2 == length.leight3)
        return true;
    else
        return false;
}
bool Triangle::isEquilateral()
{
    TriangleLength length = TriangleLength::calculate(*this);
    if (length.leight1 == length.leight2 &&
        length.leight3 == length.leight1 &&
        length.leight2 == length.leight3)
        return true;
    else
        return false;
}

/*****************************************************************************/

// ... TODO ...

/*****************************************************************************/
